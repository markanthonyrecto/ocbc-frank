'use strict';
var ocbc = {
	width: 0,
	height: 0,
	isMobile: false,
	mobileWidth: 810,
	init: function() {
		ocbc.mobileDetect();
		// for mobile
		if (ocbc.isMobile) {
			document.querySelector('html').classList.add('bp-touch');
		}

		ocbc.resize();
		window.addEventListener('resize', function() {
			ocbc.resize();

			clearTimeout(ocbc.resizeTimer);
			ocbc.resizeTimer = setTimeout(function() {
				ocbc.resize();
			}, 400);
		});

		document.addEventListener('DOMContentLoaded', function() {
			ocbc.ready();
		});

		ocbc.inview();

		ocbc.header();

		$('.com_list-switcher').each(function() {
			var _openLink = this.getAttribute('open-link'),
			_newTab = this.getAttribute('new-tab');

			ocbc.listSwitcher({
				target: this,
				openLink: _openLink,
				newTab: _newTab
			});
		});

		$('.st-fe-slider').each(function() {
			var _this = $(this),
			_imgSlider = $('.img-slider', _this),
			_descSlider = $('.desc-slider', _this);

			_imgSlider.flexslider({
				animation: 'fade',
				directionNav: false,
			    controlNav: false,
			    animationLoop: false,
			    slideshow: false,
			    itemWidth: 140,
			    itemMargin: 0,
			    asNavFor: _descSlider
			});

			_descSlider.flexslider({
				animation: 'slide',
				directionNav: true,
				controlNav: true,
				animationLoop: true,
				slideshow: true,
				slideshowSpeed: 5000,
				prevText: '',
				nextText: '',
				sync: _imgSlider
			});

			var _bulletNav = $('.flex-control-nav', _descSlider),
			_directionNav = $('.flex-direction-nav', _descSlider);

			_this.append(_bulletNav);
			_this.append(_directionNav);
		});

		$('.com_accordion').each(function() {
			var _closeOthers = this.getAttribute('close-others'),
			_openFirst = this.getAttribute('open-first');

			ocbc.accordion({
				target: this,
				closeOthers: _closeOthers,
				openFirst: _openFirst
			});
		});

		$('.com_timeline-slider').each(function() {
			ocbc.timelineSlider({
				target: this
			});
		});
	},
	ready: function() {
		ocbc.resize();

		setTimeout(function() {
			ocbc.dispatchEvent(window, 'resize');
		}, 200);

		$('.com_card-slider').each(function() {
			var _direction = this.getAttribute('direction'),
			_reverse = this.getAttribute('reverse');

			ocbc.cardSlider({
				target: this,
				direction: _direction,
				reverse: _reverse
			});
		});

		$('.com_mobile-carousel').each(function() {
			var _this = this;

			ocbc.enquire({
				between: ocbc.mobileWidth,
				desktop: function() {
					$(_this).removeClass('owl-carousel');

					$(_this).owlCarousel('destroy');
				},
				mobile: function() {
					$(_this).addClass('owl-carousel');

					$(_this).owlCarousel({
						items: 1,
						margin: 15,
						nav: false,
						dots: false,
						smartSpeed: 700
					});
				}
			});
		});
	},
	resizeTimer: 0,
	resize: function() {
		var _resize = {
			init: function() {
				ocbc.width = window.innerWidth;
				ocbc.height = window.innerHeight;

				// STICKY FOOTER
				var header = document.querySelector('.header-content'),
				footer = document.querySelector('footer');

				var headerHeight = header.offsetHeight,
				footerHeight = footer.offsetHeight;

				$(footer).css({marginTop: -(footerHeight)});
				$('#main-wrapper').css({paddingTop: headerHeight, paddingBottom: footerHeight});
				$('.com_header-height').css({paddingTop: headerHeight});

				// for equal height
				$('.group-height').each(function() {
					ocbc.equalize(this.querySelectorAll('.gh1'));
					ocbc.equalize(this.querySelectorAll('.gh2'));
				});
			}
		}
		_resize.init();
	},
	equalize: function(target) {
		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = 0;
		}

		var _biggest = 0;
		for (var i = 0; i < target.length; i++ ){
			var element_height = target[i].offsetHeight;
			if(element_height > _biggest ) _biggest = element_height;
		}

		for (var i = 0; i < target.length; i++) {
			target[i].style.minHeight = _biggest + 'px';
		}
		
		return _biggest;
	},
	inview: function(state) {
		var _animate = {
			init: function() {
				$('.animate').each(function() {
					var _state = true,
					_this = $(this),
					_animControl = _this.attr('anim-control'),
					_animName = _this.attr('anim-name'),
					_animDelay = Math.floor(_this.attr('anim-delay')),
					_delayIncrease = 0.2,
					_delayNum = 0;

					// set the css animation name
					if(!(_animName)) {
						_animName = 'anim-content';
					} else {
						// if there is a special animation name
						_animName = _animName + ' animated';
					}

					// set the animation delay
					if(!(_animDelay)) {
						// if there is no attr for delay
						_animDelay = 0.5;
					}
					_delayNum = _animDelay + 's';

					if(_animControl == 'parent') {
						// for parent container
						var _childen = $('> *', _this);

						_childen.each(function(i) {
							var _child = $(this);

							// multiply the delay depending on child count
							_delayNum = (_animDelay + (_delayIncrease * i)) + 's';

							_child.css('animation-delay', _delayNum);

							// once the target is on display
							$(_this).on('inview', function(event, visible) {
								if(_state) {
									_state = false;

									if(visible) {
										$(_this).removeClass('animate');
										_childen.addClass('animate');
										_childen.addClass(_animName + ' visible');

										// once done, remove everything related to animation
										setTimeout(function() {
											_childen.removeClass('animate animated anim-content visible');
											_childen.removeClass(_animName);
											_childen.css('animation-delay', '');
										}, ((_delayIncrease * _childen.length) * 1000) + (_animDelay * 1000) * 2);
									}
								}
							});
						});
					} else {
						// for single container
						_this.css('animation-delay', _delayNum);
						
						// once the target is on display
						$(_this).on('inview', function(event, visible) {
							if(_state) {
								_state = false;

								if(visible) {
									$(_this).addClass(_animName + ' visible');

									// once done, remove everything related to animation
									setTimeout(function() {
										$(_this).removeClass('animate animated anim-content visible');
										$(_this).removeClass(_animName);
										$(_this).css('animation-delay', '');
									}, (_animDelay * 1000) * 2);
								}
							}
						});
					}
				});
			}
		}

		if(state == 'destroy') {
			$('.animate').removeClass('animate');
		} else {
			(function(d){var p={},e,a,h=document,i=window,f=h.documentElement,j=d.expando;d.event.special.inview={add:function(a){p[a.guid+"-"+this[j]]={data:a,$element:d(this)}},remove:function(a){try{delete p[a.guid+"-"+this[j]]}catch(d){}}};d(i).bind("scroll resize",function(){e=a=null});!f.addEventListener&&f.attachEvent&&f.attachEvent("onfocusin",function(){a=null});setInterval(function(){var k=d(),j,n=0;d.each(p,function(a,b){var c=b.data.selector,d=b.$element;k=k.add(c?d.find(c):d)});if(j=k.length){var b;
			if(!(b=e)){var g={height:i.innerHeight,width:i.innerWidth};if(!g.height&&((b=h.compatMode)||!d.support.boxModel))b="CSS1Compat"===b?f:h.body,g={height:b.clientHeight,width:b.clientWidth};b=g}e=b;for(a=a||{top:i.pageYOffset||f.scrollTop||h.body.scrollTop,left:i.pageXOffset||f.scrollLeft||h.body.scrollLeft};n<j;n++)if(d.contains(f,k[n])){b=d(k[n]);var l=b.height(),m=b.width(),c=b.offset(),g=b.data("inview");if(!a||!e)break;c.top+l>a.top&&c.top<a.top+e.height&&c.left+m>a.left&&c.left<a.left+e.width?
			(m=a.left>c.left?"right":a.left+e.width<c.left+m?"left":"both",l=a.top>c.top?"bottom":a.top+e.height<c.top+l?"top":"both",c=m+"-"+l,(!g||g!==c)&&b.data("inview",c).trigger("inview",[!0,m,l])):g&&b.data("inview",!1).trigger("inview",[!1])}}},250)})(jQuery);

			document.addEventListener('DOMContentLoaded', function() {
				_animate.init();
			});
		}
	},
	mobileDetect: function() {
		var isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			ocbc.isMobile = isMobile.any();
		}
	},
	dispatchEvent: function(elem, eventName) {
		var event;
		if (typeof(Event) === 'function') {
			event = new Event(eventName);
		}
		else {
			event = document.createEvent('Event');
			event.initEvent(eventName, true, true);
		}
		elem.dispatchEvent(event);
	},
	enquire: function(opt) {
		var _enquire = {
			status: 'once',
			desktop: opt.desktop,
			mobile: opt.mobile,
			counterDesktop: 0,
			counterMobile: 0,
			width: 0,
			between: 810,
			init: function() {
				if(opt.between) {
					_enquire.between = opt.between;
				}

				if(opt.status) {
					_enquire.status = opt.status;
				}

				window.addEventListener('resize', function() {
					_enquire.update();
				});
				_enquire.update();
			},
			update: function() {
				_enquire.width = $(window).outerWidth();

				if(_enquire.status == 'always') {
					// update every resize
					if(_enquire.width > _enquire.between) {
						_enquire.desktop();
					} else {
						_enquire.mobile();
					}
				} else {
					// if detected desktop or mobile then update once
					if(_enquire.width > _enquire.between) {
						_enquire.counterMobile = 0;

						// if desktop
						if(_enquire.counterDesktop == 0) {
							_enquire.counterDesktop = 1;
							_enquire.desktop();
						}
					} else {
						_enquire.counterDesktop = 0;

						// if mobile
						if(_enquire.counterMobile == 0) {
							_enquire.counterMobile = 1;
							_enquire.mobile();
						}
					}
				}
			}
		}
		_enquire.init();
	},
	header: function() {
		var _header = {
			target: '',
			main: '',
			bottom: '',
			init: function() {
				_header.target = document.querySelector('.header-content');
				_header.main = _header.target.querySelector('.header-main');
				_header.bottom = _header.target.querySelector('.header-bottom');

				_header.resize();
				window.addEventListener('resize', function() {
					_header.resize();
					
					clearTimeout(_header.resizeTimer);
					_header.resizeTimer = setTimeout(function() {
						_header.resize();
					}, 200);
				});

				_header.scrolling();
				window.addEventListener('scroll', function() {
					_header.scrolling();					
				}, false);

				$(_header.target).mouseenter(function() {
					if(ocbc.width > ocbc.mobileWidth) {
						$(this).addClass('header-hovered');
					} else {
						$(this).removeClass('header-hovered');
					}
				}).mouseleave(function() {
					$(this).removeClass('header-hovered');
				});

				_header.menu.init();
				_header.menuMobile.init();

				// move the sub menus to subContainer
				$(_header.menu.list).each(function() {
					var _thisLi = this,
					_thisSub = _thisLi.querySelector('.hm-su-set');

					if(_thisSub) {
						// put class for the <li> with sub menu
						$(_thisLi).addClass('sub');
						
						// move this sub menu to special container for all the sub menus
						_header.menu.subContainer.appendChild(_thisSub);
					}
				});
			},
			lastScrollTop: 0,
			scrollState: true,
			scrolling: function() {
				var st = window.pageYOffset || document.documentElement.scrollTop;

				if (st > _header.lastScrollTop){
					// scrolling down
					if(st > _header.main.offsetHeight) {
						$(_header.target).addClass('header-down');

						// pull up the header once scroll down
						if($(_header.target).hasClass('header-absolute')) {
							TweenMax.set(_header.target, {y: -(_header.main.offsetHeight)});
						} else {
							if(ocbc.width > ocbc.mobileWidth) {
								// pull up the header base on header mains height
								TweenMax.to(_header.target, 0.3, {y: -(_header.main.offsetHeight + 1)});
							} else {
								// if mobile, pull base on header bottoms height
								TweenMax.to(_header.target, 0.3, {y: -(_header.bottom.offsetHeight)});
							}
						}

						$(_header.target).removeClass('header-absolute');

						// show the header bottom once scrolled down
						if(ocbc.width > ocbc.mobileWidth) {
							$(_header.bottom).slideDown(250);
						} else {
							// if in mobile, dont animate, just show
							$(_header.bottom).show();
						}

						// trigger the resize once scrolled down
						if(_header.scrollState) {
							_header.scrollState = false;

							ocbc.dispatchEvent(window, 'resize');
						}
					} else {
						$(_header.target).addClass('header-absolute');
					}
				} else {
					// dont trigger scroll up if current and last offset value are equal
					if(st != _header.lastScrollTop) {
						// scrolling up
						if(st < _header.main.offsetHeight) {
							// if scrolling on the top part of the page
							$(_header.target).removeClass('header-down');

							if(!(_header.scrollState)) {
								_header.scrollState = true;
							}
						} else {
							if(ocbc.width > ocbc.mobileWidth) {
								// if scrolling up, show header main
								TweenMax.to(_header.target, 0.3, {y: 0});
							} else {
								// if mobile, header should always be on the top
								if(st < _header.target.offsetHeight) {
									TweenMax.to(_header.target, 0.3, {y: 0});
								} else {
									// if scrolling on the part of the page, show the header bottom
									TweenMax.to(_header.target, 0.3, {y: -(_header.bottom.offsetHeight)});
								}
							}
						}

						// if scrolling on the part of the page, hide the header bottom
						if(st < (_header.main.offsetHeight * 2)) {
							if(ocbc.width > ocbc.mobileWidth) {
								$(_header.bottom).slideUp(250);
							} else {
								// if mobile, dont animate, just show
								$(_header.bottom).show();
							}
						}
					}
				}

				_header.lastScrollTop = st <= 0 ? 0 : st;
			},
			resizeTimer: 0,
			resize: function() {
				var _parent = document.querySelector('header');
				if(!(_parent)) {
					_parent = _header.target.parentNode;
				}

				if(ocbc.width > ocbc.mobileWidth) {
					$(_parent).css({paddingTop: ''});

					$(_header.target).css({paddingTop: ''});
				} else {
					$(_parent).css({paddingTop: _header.bottom.offsetHeight});

					$(_header.target).css({paddingTop: _header.bottom.offsetHeight});
				}

				var _subMenu = _header.target.querySelector('.header-menu-sub');
				$(_subMenu).css({paddingTop: _header.main.offsetHeight});
			},
			menu: {
				target: '',
				list: '',
				subContainer: '',
				active: false,
				init: function() {
					_header.menu.target = _header.target.querySelector('.menu');
					_header.menu.list = $('> ul > li', _header.menu.target);
					_header.menu.subContainer = _header.target.querySelector('.header-menu-sub .container');

					$(_header.menu.list).each(function() {
						var _thisLi = this,
						_thisSub = _thisLi.querySelector('.hm-su-set');

						// when hovering on this <li>
						$(_thisLi).mouseenter(function() {
							if(ocbc.width > ocbc.mobileWidth) {
								if(_thisSub) {
									_header.menu.show(_thisLi, _thisSub);
								} else {
									// if this <li> dont have sub menu, then hide the sub menu container
									_header.menu.hide();
								}
							}
						});
					});

					// hide the sub menu if outside the header
					$(window).mouseenter(function(e) {
						if($(e.target).closest(_header.target).length == 0) {
							_header.menu.hide();
						}
					});

					// hide the sub menu if outside the menu or sub menu container
					$(document).mousemove(function(e){
						if(e.pageX < _header.menu.target.offsetLeft - 70 || e.pageX > _header.menu.target.offsetLeft + _header.menu.target.offsetWidth) {
							if($(e.target).closest(_header.menu.subContainer.parentNode).length == 0) {
								_header.menu.hide();
							}
						}

						if(e.pageY < _header.menu.target.offsetTop - 10) {
							_header.menu.hide();
						}
					});

					// move this menu depending on desktop/mobile display
					var _menuParent = _header.menu.target.parentNode,
					_mobileMenuParent = _header.target.querySelector('.header-menu-mobile');
					ocbc.enquire({
						between: ocbc.mobileWidth,
						desktop: function() {
							_menuParent.appendChild(_header.menu.target);

							$(_header.bottom).hide();
						},
						mobile: function() {
							_mobileMenuParent.appendChild(_header.menu.target);

							$(_header.bottom).show();
						}
					});
				},
				show: function(target, sub) {
					_header.resize();

					$(_header.menu.list).removeClass('sub-active');
					$('.hm-su-set', _header.menu.subContainer).hide();

					if(sub) {
						$(target).addClass('sub-active');
					}

					$(sub).show();

					if(!(_header.menu.active)) {
						TweenMax.set(sub, {opacity: 0});

						$(_header.menu.subContainer).parent().slideDown(250, function() {
							_header.menu.active = true;

							TweenMax.to(sub, 0.5, {opacity: 1});
						});
					}
				},
				hide: function(target, sub) {
					if(_header.menu.active) {
						_header.menu.active = false;

						$(_header.menu.list).removeClass('sub-active');

						$(_header.menu.subContainer).parent().slideUp(250);
					}
				}
			},
			menuMobile: {
				bar: '',
				subMenu: '',
				init: function() {
					_header.menuMobile.bar = _header.target.querySelector('.menu-bar');
					_header.menuMobile.subMenu = _header.target.querySelector('.header-menu-mobile');

					$(_header.menuMobile.bar).click(function() {
						if($(this).hasClass('active')) {
							_header.menuMobile.hide();
						} else {
							if($(_header.target).hasClass('header-down')) {
								_header.menuMobile.show();
							} else {
								$('body, html').animate({scrollTop: $('#main-wrapper').offset().top + (_header.main.offsetHeight / 2)}, 500, function() {
									_header.menuMobile.show();
								});
							}
						}
					});

					_header.menuMobile.resize();
					window.addEventListener('resize', function() {
						_header.menuMobile.resize();

						clearTimeout(_header.menuMobile.resizeTimer);
						_header.menuMobile.resizeTimer = setTimeout(function() {
							_header.menuMobile.resize();
						}, 400);
					});

					ocbc.enquire({
						between: ocbc.mobileWidth,
						desktop: function() {
							_header.menuMobile.hide();
						},
						mobile: function() {
							_header.menuMobile.hide();
						}
					});

					$(_header.menu.list).each(function() {
						var _thisLi = this,
						_thisSub = _thisLi.querySelector('.hm-su-set');

						if(_thisSub) {
							$(_thisLi).click(function(e) {
								if(ocbc.width < 810 || ocbc.width == 810) {
									e.preventDefault();
								}

								if(ocbc.width < ocbc.mobileWidth || ocbc.width == ocbc.mobileWidth) {
									_header.menuMobile.showSubMenu(this, _thisSub);
								}
							});
						}
					});
				},
				show: function() {
					$('html').addClass('menu-mobile-active');
					$(_header.menuMobile.bar).addClass('active');

					var _height = window.innerHeight - _header.main.offsetHeight;
					TweenMax.set(_header.menuMobile.subMenu, {opacity: 0, display: 'block', height: 0, x: 0});
					TweenMax.to(_header.menuMobile.subMenu, 0.3, {opacity: 1, height: _height});

					TweenMax.set($('> ul > li', _header.menu.target), {opacity: 0, y: 50});
					TweenMax.staggerTo($('> ul > li', _header.menu.target), 0.5, {opacity: 1, y: 0, onComplete: function() {
						TweenMax.set($('> ul > li', _header.menu.target), {clearProps: 'all'});
					}}, 0.05);
				},
				hide: function() {
					$('html').removeClass('menu-mobile-active');
					$(_header.menuMobile.bar).removeClass('active');

					TweenMax.to(_header.menuMobile.subMenu, 0.3, {opacity: 0, height: 0, onComplete: function() {
						$(_header.menuMobile.subMenu).hide();
					}});

					TweenMax.to(_header.menu.subContainer.parentNode, 0.3, {height: 0, clearProps: 'all', onComplete: function() {
						$('.menu-back-btn', _header.menu.subContainer.parentNode).remove();
					}});
				},
				showSubMenu: function(target, sub) {
					$('.hm-su-set', _header.menu.subContainer).hide();

					$(sub).show();

					TweenMax.to(_header.menuMobile.subMenu, 0.3, {x: -50, delay: 0.1});

					TweenMax.set(_header.menu.subContainer.parentNode, {opacity: 0, display: 'block', x: window.innerWidth, top: (_header.main.offsetHeight * 2), height: window.innerHeight - _header.main.offsetHeight, paddingTop: 40});
					TweenMax.to(_header.menu.subContainer.parentNode, 0.3, {opacity: 1, x: 0});

					var _backBtn = _header.menu.subContainer.parentNode.querySelector('.menu-back-btn');
					if(_backBtn) {
						$(_backBtn).remove();
					}

					_backBtn = document.createElement('a');
					$(_backBtn).addClass('link menu-back-btn');
					$(_backBtn).html('<i class="fal fa-angle-left"></i> ' + target.querySelector('a').innerHTML);
					$(_header.menu.subContainer.parentNode).prepend(_backBtn);

					_backBtn.addEventListener('click', function(e) {
						e.preventDefault();
						_header.menuMobile.hideSubMenu();
					});
				},
				hideSubMenu: function() {
					TweenMax.to(_header.menuMobile.subMenu, 0.3, {x: 0});

					TweenMax.to(_header.menu.subContainer.parentNode, 0.3, {opacity: 0, x: window.innerWidth});
				},
				resizeTimer: 0,
				resize: function() {
					if($('html').hasClass('menu-mobile-active')) {
						var _height = window.innerHeight - _header.main.offsetHeight;

						$(_header.menuMobile.subMenu).css({height: _height});
						$(_header.menu.subContainer.parentNode).css({height: _height, paddingTop: 40});
					}
				}
			}
		}
		_header.init();
	},
	listSwitcher: function(opt) {
		var _switcher = {
			target: '',
			select: '',
			list: '',
			indicator: '',
			openLink: false,
			newTab: false,
			init: function() {
				_switcher.target = opt.target;
				_switcher.select = _switcher.target.querySelector('select');

				if(opt.openLink) {
					if(opt.openLink == 'true') {
						_switcher.openLink = true;
					} else {
						_switcher.openLink = false;
					}
				}

				if(opt.newTab) {
					if(opt.newTab == 'true') {
						_switcher.newTab = true;
					} else {
						_switcher.newTab = false;
					}
				}

				_switcher.createList();
				
				_switcher.update('set');
				window.addEventListener('resize', function() {
					_switcher.update();

					clearTimeout(_switcher.updateTimer);
					_switcher.updateTimer = setTimeout(function() {
						_switcher.update();
					}, 400);
				});
			},
			createList: function() {
				if(_switcher.select) {
					// create <ul>
					var _curList = _switcher.target.querySelector('ul');
					if(_curList) {
						_switcher.target.removeChild(_curList);
					}

					_switcher.list = document.createElement('ul');

					// create <li>
					for (var i = 0; i < _switcher.select.options.length; i++) {
						var _li = document.createElement('li'),
						_a = document.createElement('a');
						_a.innerHTML = _switcher.select.options[i].innerHTML;
						_a.href = '#';

						_li.appendChild(_a);
						_switcher.list.appendChild(_li);
					}

					_switcher.target.appendChild(_switcher.list);
				} else {
					_switcher.list = _switcher.target.querySelector('ul');
				}

				// create --indicator
				_switcher.indicator = document.createElement('div');
				_switcher.indicator.classList.add('com_list-switcher--indicator');
				_switcher.target.appendChild(_switcher.indicator);

				var _list = _switcher.list.querySelectorAll('li');

				for (var i = 0; i < _list.length; i++) {
					if(_switcher.select) {
						var _index = i;
						_list[i].setAttribute('index', i);

						// put active to <li> base on selectedIndex
						if(i == _switcher.select.selectedIndex) {
							_list[i].classList.add('active');
						}
					} else {
						_list[0].classList.add('active');
					}

					// when clicking <a>
					_list[i].querySelector('a').addEventListener('click', function(e) {
						e.preventDefault();
						var _parent = this.parentNode;

						if(!(_parent.classList.contains('active'))) {
							// change the active <li>
							var _currentActive = _switcher.list.querySelector('li.active');
							_currentActive.classList.remove('active');

							_parent.classList.add('active');

							// change the selectedIndex
							if(_switcher.select) {
								_switcher.select.selectedIndex = _parent.getAttribute('index');
							}

							_switcher.update('update', 'click');
						}
					});
				}
			},
			updateTimer: 0,
			update: function(state, updateEvent) {
				if(!(state)) {
					if(_switcher.indicator.offsetWidth == 0) {
						state = 'set';
					}
				}

				// trigger event change on <select>
				if(_switcher.select) {
					_switcher.select.dispatchEvent(new Event('change'));
				}

				var _list = _switcher.list.querySelectorAll('li'),
				_height = 0;

				for (var i = 0; i < _list.length; i++) {
					var _thisHeight = _list[i].offsetHeight;

					if(_thisHeight > _height) {
						_height = _thisHeight;
					}
				}

				var _active = _switcher.target.querySelector('li.active');

				// animate the --indicator position
				if(state == 'set') {
					TweenMax.set(_switcher.indicator, {x: _active.offsetLeft, y: _active.parentNode.offsetTop, width: _active.offsetWidth, height: _height});
				} else {
					TweenMax.to(_switcher.indicator, 0.3, {x: _active.offsetLeft, y: _active.parentNode.offsetTop, width: _active.offsetWidth, height: _height, onComplete: function() {
						// open the link
						if(updateEvent == 'click') {
							if(_switcher.openLink) {
								var _a = _active.querySelector('a');

								if(_switcher.newTab) {
									window.open(_a.href, '_blank');
								} else {
									window.open(_a.href, '_self');
								}
							}
						}
					}});
				}
			}
		}
		_switcher.init();
	},
	accordion: function(opt) {
		var _accordion = {
			target: '',
			closeOthers: true,
			openFirst: false,
			list: '',
			init: function() {
				_accordion.target = opt.target;

				if(opt.closeOthers) {
					if(opt.closeOthers == 'true') {
						_accordion.closeOthers = true;
					} else {
						_accordion.closeOthers = false;
					}
				}

				if(opt.openFirst) {
					if(opt.openFirst == 'true') {
						_accordion.openFirst = true;
					} else {
						_accordion.openFirst = false;
					}
				}

				_accordion.list = _accordion.target.querySelectorAll('.com_accordion-set');
				for (var i = 0; i < _accordion.list.length; i++) {
					var _this = _accordion.list[i],
					_title = _this.querySelector('.com_accordion-title');

					_title.addEventListener('click', function(e) {
						e.preventDefault();

						if(this.parentNode.classList.contains('selected')) {
							_accordion.close(this.parentNode);
						} else {
							_accordion.open(this.parentNode);
						}
					});
				}

				// open the default selected sets
				var _activeSet = _accordion.target.querySelectorAll('.com_accordion-set.selected');
				if(_activeSet) {
					for (var i = 0; i < _activeSet.length; i++) {
						_accordion.open(_activeSet[i]);
					}
				}

				if(_accordion.openFirst) {
					_accordion.open(_accordion.list[0]);
				}
			},
			open: function(target) {
				if(_accordion.closeOthers) {
					var _activeSet = _accordion.target.querySelectorAll('.com_accordion-set.selected');

					if(_activeSet) {
						for (var i = 0; i < _activeSet.length; i++) {
							_accordion.close(_activeSet[i]);
						}
					}
				}

				target.classList.add('selected');

				var _content = target.querySelector('.com_accordion-content');
				$(_content).slideDown(250);
			},
			close: function(target) {
				target.classList.remove('selected');

				var _content = target.querySelector('.com_accordion-content');
				$(_content).slideUp(250);
			}
		}
		_accordion.init();
	},
	cardSlider: function(opt) {
		var _slider = {
			target: '',
			wrapper: '',
			list: '',
			slides: [],
			itemWidth: 0,
			itemHeight: 0,
			wrapWidth: 0,
			direction: 'right',
			reverse: false,
			init: function() {
				_slider.target = opt.target;

				if(opt.direction) {
					if(opt.direction = 'left') {
						_slider.direction = 'left'
					} else {
						_slider.direction = 'right'
					}
				}

				if(opt.reverse) {
					if(opt.reverse == 'true') {
						_slider.reverse = true;
					} else {
						_slider.reverse = false;
					}
				}

				// create containers
				_slider.create();

				_slider.resize();
				window.addEventListener('resize', function() {
					_slider.resize();

					clearTimeout(_slider.resizeTimer);
					_slider.resizeTimer = setTimeout(function() {
						_slider.resize();
					}, 400);
				});

				// run the animation
				_slider.animation();
			},
			create: function() {
				var _ul = $('> ul', _slider.target);

				// create dragger
				_slider.dragger = document.createElement('div');

				// create <ul>
				_slider.list = document.createElement('ul');
				$(_slider.list).addClass('list');

				// get the <li>
				var _li = _slider.target.querySelectorAll('li');
				for (var i = 0; i < _li.length; i++) {
					_slider.list.append(_li[i]);
					_slider.slides.push(_li[i]);
				}

				// duplicate the <li>
				for (var i = 0; i < _li.length; i++) {
					var _thisLi = document.createElement('li');
					$(_thisLi).html($(_slider.slides[i]).html());
					
					_slider.list.append(_thisLi);
					_slider.slides.push(_thisLi);
				}
				_ul.remove();

				// if list are reverse
				if(_slider.reverse) {
					$('li', _slider.list).remove();
					_slider.slides.reverse();

					for (var i = 0; i < _slider.slides.length; i++) {
						_slider.list.appendChild(_slider.slides[i]);
					}
				}

				// create main wrapper
				_slider.wrapper = document.createElement('div');
				$(_slider.wrapper).addClass('slider-wrapper');

				_slider.wrapper.appendChild(_slider.list);
				_slider.target.appendChild(_slider.wrapper);

				// show the slider
				$(_slider.target).addClass('slider-active');
			},
			resizeTimer: 0,
			resize: function() {
				_slider.itemWidth = 0;
				_slider.itemHeight = 0;

				// get the common width and height of items
				for (var i = 0; i < _slider.slides.length; i++) {
					var _thisWidth = $(_slider.slides[i]).outerWidth(),
					_thisHeight = $(_slider.slides[i]).outerHeight();

					if(_slider.itemWidth < _thisWidth) {
						_slider.itemWidth = _thisWidth;
					}

					if(_slider.itemHeight < _thisHeight) {
						_slider.itemHeight = _thisHeight;
					}
				}

				_slider.wrapWidth = _slider.slides.length * (_slider.target.offsetWidth / (_slider.target.offsetWidth / _slider.itemWidth));

				$(_slider.list).css({minHeight: _slider.itemHeight});
				
				if(_slider.direction == 'right') {
					$(_slider.list).css({left: -(_slider.itemWidth + (_slider.itemWidth / 1.55))});
				} else {
					$(_slider.list).css({left: -(_slider.itemWidth + (_slider.itemWidth / 5))});
				}
			},
			animation: function() {
				// put x position and width for its item
				for (var i = 0; i < _slider.slides.length; i++) {
					TweenLite.set(_slider.slides[i], {x: i * _slider.itemWidth, width: _slider.itemWidth});
				}

				// update the x position of every items
				var animate = TweenMax.to(_slider.slides, 1, {
					x: "+=" + _slider.wrapWidth,
					ease: Linear.easeNone,
					paused: true,
					repeat: -1,
					modifiers: {
						x: function(x) {
							x %= _slider.wrapWidth;
							return x
						}
					}
				});

				var _draggerUpdate = function() {
					// console.log('drag', _dragger[0].x);
					animate.progress(_dragger[0].x / _slider.wrapWidth);
				}

				// create draggable
				var _dragger = Draggable.create(_slider.dragger, {
					type: 'x',
					trigger: _slider.wrapper,
					throwProps: true,
					onDrag: _draggerUpdate,
					onThrowUpdate: _draggerUpdate
				});

				// control scroll once inview
				var _visible = false;
				$(_slider.target.parentNode).on('inview', function(event, visible) {
					if(visible) {
						_visible = true;
					} else {
						_visible = false;
					}
				});

				// animate the slider upon page scrolling
				var _lastScrollTop = 0;
				var _scale = _slider.slides.length * 2;
				var _scrolling = function() {
					var st = window.pageYOffset || document.documentElement.scrollTop;

					if(st < _lastScrollTop) {
						// scrolling up
						if(_slider.direction == 'right') {
							_scrollX = _dragger[0].x - (_scale);
						} else {
							_scrollX = _dragger[0].x + (_scale);
						}

					} else {
						// scrolling down
						if(_slider.direction == 'right') {
							_scrollX = _dragger[0].x + (_scale);
						} else {
							_scrollX = _dragger[0].x - (_scale);
						}
						
					}

					// update the dragger left position
					_dragger[0].target._gsTransform.x = _scrollX;
					TweenLite.set(_dragger[0], {x: _scrollX, startX: _scrollX});

					_draggerUpdate();

					_lastScrollTop = st <= 0 ? 0 : st;
				}
				
				window.addEventListener('scroll', function() {
					if(_visible) {
						_scrolling();
					}
				});
			}
		}
		_slider.init();
	},
	timelineSlider: function(opt) {
		var _slider = {
			target: '',
			nav: '',
			navList: '',
			main: '',
			activeSlide: 0,
			bar: '',
			init: function() {
				_slider.target = opt.target;
				_slider.nav = _slider.target.querySelector('.nav-slider');
				_slider.navList = _slider.nav.querySelectorAll('li');
				_slider.main = _slider.target.querySelector('.main-slider');

				for (var i = 0; i < _slider.navList.length; i++) {
					var _thisNav = _slider.navList[i];

					_thisNav.setAttribute('index', i);

					_thisNav.addEventListener('click', function(e) {
						e.preventDefault();
						
						_slider.update(this.getAttribute('index'));
					});
				}

				// active the specific slide on init
				var _curActive = _slider.nav.querySelector('.active');
				if(_curActive) {
					var _index = _curActive.getAttribute('index');
					_slider.update(_index);
				} else {
					// active the first slide
					_slider.update(0);
				}

				_slider.timeline(_slider.activeSlide, 'create');
				window.addEventListener('resize', function() {
					_slider.timeline(_slider.activeSlide);

					clearTimeout(_slider.timelineTimer);
					_slider.timelineTimer = setTimeout(function() {
						_slider.timeline(_slider.activeSlide);
					}, 200);
				});
			},
			update: function(index) {
				_slider.activeSlide = index;

				// update the nav
				for (var i = 0; i < _slider.navList.length; i++) {
					var _thisNav = _slider.navList[i];

					if(i == index) {
						$(_thisNav).addClass('active');
					} else {
						$(_thisNav).removeClass('active');
					}
				}

				var _slideList = _slider.main.querySelectorAll('.slide');
				for (var i = 0; i < _slideList.length; i++) {
					var _thisSlide = _slideList[i];

					if(i == index) {
						$(_thisSlide).show();
					} else {
						$(_thisSlide).hide();
					}
				}

				_slider.timeline(index);
			},
			timelineTimer: 0,
			timeline: function(index, state) {
				if(state == 'create') {
					_slider.bar = document.createElement('div');
					var _span = document.createElement('span');

					$(_slider.bar).addClass('nav-slider-bar');
					$(_slider.bar).append(_span);

					$(_slider.nav).append(_slider.bar);
				}

				if(_slider.bar) {
					var _span = _slider.bar.querySelector('span'),
					_left = _slider.navList[index].offsetLeft + (_slider.navList[index].offsetWidth / 2) - (_span.offsetWidth / 2);

					if(index == 0) {
						_left = 0;
					}

					if(index == _slider.navList.length - 1) {
						_left = _slider.bar.offsetWidth - (_span.offsetWidth / 2);
					}

					TweenMax.to(_span, 0.1, {x: _left});
				}
			}
		}
		_slider.init();
	}
}
ocbc.init();